Assets {
  Id: 16910278292812118833
  Name: "Sun Light"
  PlatformAssetType: 6
  PrimaryAsset {
    AssetType: "BlueprintAssetRef"
    AssetId: "CORESKY_SunLight"
  }
}
Assets {
  Id: 14580297661185880422
  Name: "Sword Swipe Full Circle"
  PlatformAssetType: 8
  PrimaryAsset {
    AssetType: "VfxBlueprintAssetRef"
    AssetId: "fxbp_sword_swipe_02"
  }
}
Assets {
  Id: 13176689984730082630
  Name: "Bench Full"
  PlatformAssetType: 1
  PrimaryAsset {
    AssetType: "StaticMeshAssetRef"
    AssetId: "sm_bench_001"
  }
}
Assets {
  Id: 12095835209017042614
  Name: "Cube"
  PlatformAssetType: 1
  PrimaryAsset {
    AssetType: "StaticMeshAssetRef"
    AssetId: "sm_cube_002"
  }
}
Assets {
  Id: 11515840070784317904
  Name: "Skylight"
  PlatformAssetType: 6
  PrimaryAsset {
    AssetType: "BlueprintAssetRef"
    AssetId: "CORESKY_Skylight"
  }
}
Assets {
  Id: 7887238662729938253
  Name: "Sky Dome"
  PlatformAssetType: 6
  PrimaryAsset {
    AssetType: "BlueprintAssetRef"
    AssetId: "CORESKY_Sky"
  }
}
Assets {
  Id: 4801985963367974663
  Name: "Fantasy Sword Blade 03"
  PlatformAssetType: 1
  PrimaryAsset {
    AssetType: "StaticMeshAssetRef"
    AssetId: "sm_weap_fan_blade_sword_003"
  }
}
Assets {
  Id: 3694419892625887874
  Name: "Military Hazard Barrel"
  PlatformAssetType: 1
  PrimaryAsset {
    AssetType: "StaticMeshAssetRef"
    AssetId: "sm_prop_mil_barrel_01_ref"
  }
}
Assets {
  Id: 3682206342183528038
  Name: "Fantasy Sword Grip 01"
  PlatformAssetType: 1
  PrimaryAsset {
    AssetType: "StaticMeshAssetRef"
    AssetId: "sm_weap_fan_grip_sword_001"
  }
}
Assets {
  Id: 1920010878601219178
  Name: "Fantasy Sword Guard 01"
  PlatformAssetType: 1
  PrimaryAsset {
    AssetType: "StaticMeshAssetRef"
    AssetId: "sm_weap_fan_guard_sword_001"
  }
}
Assets {
  Id: 1383772742094820961
  Name: "Fantasy Pommel 01"
  PlatformAssetType: 1
  PrimaryAsset {
    AssetType: "StaticMeshAssetRef"
    AssetId: "sm_weap_fan_pommel_001"
  }
}
