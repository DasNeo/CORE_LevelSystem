local propClasses = script:GetCustomProperty("Classes")
classes = require(propClasses)

levels = {
    classes.Level:create(10, "Tester 1"),
    classes.Level:create(10, "Tester 2"),
    classes.Level:create(10, "Tester 3"),
    classes.Level:create(20, "Tester 4"),
    classes.Level:create(20, "Tester 5"),
    classes.Level:create(20, "Tester 6"),
    classes.Level:create(20, "Tester 7"),
    classes.Level:create(20, "Tester 8"),
    classes.Level:create(20, "Tester 9"),
    classes.Level:create(20, "Tester 10"),
    classes.Level:create(20, "Tester 11"),
    classes.Level:create(110, "Tester 12"),
    classes.Level:create(245, "Tester 13"),
    classes.Level:create(110, "Tester 14"),
    classes.Level:create(100, "Tester 15"),
    classes.Level:create(100, "Tester 16"),
    classes.Level:create(100, "Tester 17"),
    classes.Level:create(100, "Tester 18"),
}

-- Calculates the percentage to the next level.
-- returns a float between 0 and 1
function CalculatePercentageToNextLevel(player, playerXP, playerLevel, levels)
    if playerXP == 0 then return 0 end
    if levels[playerLevel+1] == nil then
        print("CalculatePercentageToNextLevel - max level reached")
        return 1
    end
    reqXP = levels[playerLevel].xp_required
    
    percentage = (100 / reqXP * playerXP) / 100
    if percentage == 1 and CanLevelUp(player, playerXP, playerLevel) then
        return 0
    end
    return percentage
end

-- Check if the player can level up given the XP and level
function CanLevelUp(playerXP, playerLevel)
    if levels[playerLevel+1] ~= nil and playerXP >= levels[playerLevel].xp_required then
        return true
    end
    return false
end

-- AddXP(player, 10)
-- Give a player some xp
-- the third parameter is only used internally!
-- it's used to recursivly set the level to the highest we can reach without firing 
-- SetResources Events for the XP changes.
-- (If e.g. you receive 1000 XP and only need 10 xp each for 1 level
-- there'd be xp changes for every level from 0 to xp_required)
-- So we only get a bunch of SetResources Events for the Level and not for the xp
-- This was done to reduce the amount of Broadcasts we send
function AddXP(player, xpToAdd, xp)
    if not player:IsA("Player") then return end
    playerXP = xp or player:GetResource("xp")
    playerLevel = player:GetResource("level")
    if levels[playerLevel] ~= nil then
        if playerXP + xpToAdd == levels[playerLevel].xp_required then
            player:SetResource("level", playerLevel + 1)
            player:SetResource("xp", 0)
        elseif playerXP + xpToAdd > levels[playerLevel].xp_required then
            if levels[playerLevel+1] == nil then
                player:SetResource("xp", levels[#levels+1-1].xp_required)
            else
                remainingXP = xpToAdd - (levels[playerLevel].xp_required - playerXP)
                player:SetResource("level", playerLevel+1)
                
                Events.Broadcast("PlayerLevelup", player, levels[playerLevel+1])
                AddXP(player, remainingXP, 0)
            end
        else
            player:SetResource("xp", playerXP + xpToAdd)
        end
    else
        warn("level would be nil " .. playerLevel)
    end
end

local api = {}
api.Level = Level
api.levels = levels
api.CalculatePercentageToNextLevel = CalculatePercentageToNextLevel
api.AddXP = AddXP
api.Init = Init
return api