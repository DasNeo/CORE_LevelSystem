local propLevels = script:GetCustomProperty("Levels")
levels = require(propLevels)

function run()
    while true do
        levels.AddXP(player, 1)
        Task.Wait(1)
    end
end

task = nil
player = nil

function OnBeginOverlap(whichTrigger, other)
    if other:IsA("Player") then
        player = other
        task = Task.Spawn(run)
    end
end

function OnEndOverlap(whichTrigger, other)
	task:Cancel()
end

script.parent.beginOverlapEvent:Connect(OnBeginOverlap)
script.parent.endOverlapEvent:Connect(OnEndOverlap)