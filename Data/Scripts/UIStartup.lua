local propUtils = script:GetCustomProperty("Utilities")
local propLevels = script:GetCustomProperty("Levels")
util = require(propUtils)
level = require(propLevels)

local PROFILE_IMAGE = script.parent:FindDescendantByName("Avatar")
local PROFILE_NAME = script.parent:FindDescendantByName("Name")
local PROFILE_LEVEL = script.parent:FindDescendantByName("Level")
local PROFILE_XP = script.parent:FindDescendantByName("XP")
local PROFILE_XP_PROGRESS = script.parent:FindDescendantByName("Progress")

local LOCAL_PLAYER = Game.GetLocalPlayer()
local lastLevel = -1

function Redraw(PlayerLevel, LevelDescription, XP, XPPercentage, XPRequired)
    if lastLevel ~= PlayerLevel and lastLevel ~= -1 then
        lastLevel = PlayerLevel
        return 
        -- Skip one redraw cycle if the level just changed
        -- If we don't do this the Level gets redrawn before the XP (which looks shit)
    end
    lastLevel = PlayerLevel
    Task.Wait()
    local levelValue = LOCAL_PLAYER:GetResource("level")
    local levelText = "Level"
    if LevelDescription ~= nil then
        levelText = LevelDescription
        levelValueText = "(" .. levelValue .. ")"
    end
    local xp = LOCAL_PLAYER:GetResource("xp")
    PROFILE_NAME.text = LOCAL_PLAYER.name
    PROFILE_IMAGE:SetImage(LOCAL_PLAYER)
    PROFILE_LEVEL.text = util.interp("${levelName} ${levelValue}", { levelName = levelText, levelValue = levelValueText or levelValue })
    requiredXPText = ""
    if XPRequired ~= nil then
        requiredXPText = " / " .. XPRequired
    end
    PROFILE_XP.text = util.interp("XP: ${xp}${requiredXPText}", { xp = xp, requiredXPText = requiredXPText })
    PROFILE_XP_PROGRESS.progress = XPPercentage
end

Events.Connect("TriggerRedraw", Redraw)

local api = {}
api.Redraw = Redraw
return api