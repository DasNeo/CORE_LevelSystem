-- stolen from https://discord.com/channels/547553459562610698/547556380781838346/836781766983417898
function interp(s, tab)
    return (s:gsub('($%b{})', function(w) return tab[w:sub(3, -2)] or w end))
end

local api = {}
api.interp = interp
return api