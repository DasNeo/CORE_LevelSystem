local propUtils = script:GetCustomProperty("Utilities")
local propClasses = script:GetCustomProperty("Classes")
local propLevels = script:GetCustomProperty("Levels")
classes = require(propClasses)
utils = require(propUtils)
levels = require(propLevels)

function OnPlayerLevelup(player, level)
    print("Player " .. player.name .. " leveled up to level " .. level.description)
end

function OnPlayerJoined(player)
    local data = Storage.GetPlayerData(player)
    player:SetResource("level", data["level"] or 1)
    player:SetResource("xp", data["xp"] or 0)
    player.resourceChangedEvent:Connect(OnResourceChanged)

    TriggerRedraw(player, player:GetResource("xp"))
end

-- Triggers a Redraw on UI whenever XP or Level Resources get changed
function OnResourceChanged(player, resName, resValue)
    if (resName == "xp" or resName == "level") then
        local data = Storage.GetPlayerData(player)
        data[resName] = resValue
        local resultCode,errorMessage = Storage.SetPlayerData(player, data)
        print(player.name .. " received " .. resValue .. " " .. resName)
        
        if resName == "xp" then
            xp = resValue
        end
        TriggerRedraw(player, xp or player:GetResource("xp"))
    end
end

-- Broadcasts the TriggerRedraw Event to the client
function TriggerRedraw(player, xp)
    level = player:GetResource("level")
    description = levels.levels[level].description
    xprequired = levels.levels[level].xp_required
    percentage = levels.CalculatePercentageToNextLevel(player, xp, level, levels.levels)
    
    xp = xp or player:GetResource("xp")
    Events.BroadcastToPlayer(player, "TriggerRedraw", level, description, xp, percentage, xprequired)
end

Events.Connect("PlayerLevelup", OnPlayerLevelup)
Game.playerJoinedEvent:Connect(OnPlayerJoined)