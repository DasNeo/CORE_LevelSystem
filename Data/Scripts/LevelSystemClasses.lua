Level = {}
Level.__index = Level

function Level:create(xp_required, description)
    local lvl = {}
    setmetatable(lvl, Level)
    if description ~= nil then -- Optional parameter description
        lvl.description = description
    end
    lvl.xp_required = xp_required
    return lvl
end

local api = {}
api.Level = Level
return api