local propLevels = script:GetCustomProperty("Levels")
levels = require(propLevels)

-- function to connect event handlers to ability events 
function ConnectAbilityEvents_IncreaseXP(ability)
	-- hooks on entering each phase
	ability.castEvent:Connect(OnCast_IncreaseXP)
	ability.executeEvent:Connect(OnExecute_IncreaseXP)
	
	ability.recoveryEvent:Connect(OnRecovery_IncreaseXP)
		
	ability.cooldownEvent:Connect(OnCooldown_IncreaseXP)
		
	ability.interruptedEvent:Connect(OnInterrupted_IncreaseXP)
		
	ability.readyEvent:Connect(OnReady_IncreaseXP)
			
end

-- functions called when entering each phase. Add your code inside 
function OnCast_IncreaseXP(ability)
	--print("OnCast " .. ability.name)
end

function OnExecute_IncreaseXP(ability)
	--ability.owner:SetResource("level", 1)
	--ability.owner:SetResource("xp", 0)
	levels.AddXP(ability.owner, 1)
	local targetData = ability:GetTargetData()
end

function OnRecovery_IncreaseXP(ability)
	-- print("OnRecovery " .. ability.name)
end

function OnCooldown_IncreaseXP(ability)
	-- print("OnCooldown " .. ability.name)
end

function OnInterrupted_IncreaseXP(ability)
	-- print("OnInterrupted " .. ability.name)
end

function OnReady_IncreaseXP(ability)
	-- print("OnReady " .. ability.name)
end


-- reference to ability object, modify as needed
local myAbility = script.parent

-- call to connect events to ability. 
-- this does not give the ability to player, that need to be handled separately depending on how ability is created in game
ConnectAbilityEvents_IncreaseXP(myAbility)

--------------------------------------------------------------------------------